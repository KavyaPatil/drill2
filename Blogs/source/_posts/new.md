
# Truthy and Falsey Values


<p style="font-size:18px;"> In JavaScript, truthy and falsy values are used to determine the truthfulness or falseness of a value in a boolean context.</p>

<p style="font-size:18px;"> Each Values in JavaScript is either the truthy or falsey value i.e when an expression is evaluated in a boolean context (such as in an if statement or as an operand to the logical operators && and ||), JavaScript will automatically perform type coercion and treat values as truthy or falsy based on their underlying truthiness.</p>

<p style="font-size:18px;">The number of falsy values are very less. Every values other than falsy values are truthy.</p>

#### Some examples for falsy values:


<code>
const number = null;   
let name;
const value = 0;
const isTrue = false;
const stringVal = '';
const amount = NaN;

</code>

<p style="font-size:18px;">These Values can be checked whether they hold the falsy or truthy value by using the Boolean() method.</p>


<code>

Boolean(number)  //false
Boolean(name)    //false

</code>

### Some examples for truthy values:

```

const number = 20;
const name = 'Ram';
const array = [];
const object = {};


Boolean(number)  //true
Boolean(name)    //true
Boolean(array)   //true
Boolean(object)  //true

```


<p style="font-size:18px;">Truthy and falsy values are used in conditional statements like if, while, ternary operators to execute the particular piece of code.</p>

<p style="font-size:18px;">For example,</p>

```
const name = 'something';

if(name){

console.log('Hello' +name+'!' );     // Hello something!

}else{

console.log('Hello!');

}

```

<p style="font-size:18px;">In the above example name is declared and initialised with the value 'something'. If the name is checked in the 'if condition' the value is type coerced to boolean and checks true and the code in that particular block get executed.</p>

```

const result = '' ? console.log('given value is truthy') : console.log('given value is falsy') ;
// values are not equal

```

<p style="font-size:18px;"> In above instance the value '' is checked, if the given value is truthy it executes the first statement otherwise the second statement will get executed.</p>












