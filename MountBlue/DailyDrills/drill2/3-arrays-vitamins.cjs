const items = [
  {
    name: "Orange",
    available: true,
    contains: "Vitamin C",
  },
  {
    name: "Mango",
    available: true,
    contains: "Vitamin K, Vitamin C",
  },
  {
    name: "Pineapple",
    available: true,
    contains: "Vitamin A",
  },
  {
    name: "Raspberry",
    available: false,
    contains: "Vitamin B, Vitamin A",
  },
  {
    name: "Grapes",
    contains: "Vitamin D",
    available: false,
  },
];

// 1. Get all items that are available

const allItems = (items) => {
  const arrOfAllItems = items.map((item) => {
    return item.name;
  });

  return arrOfAllItems;
};

console.log(allItems(items));




// 2. Get all items containing only Vitamin C.

const itemsOnlyVitaminC = (items) => {

  const arrayOfitemsVitaminC = items.filter((item) => {

    return item.contains === "Vitamin C";
  });

  return arrayOfitemsVitaminC;
};

console.log(itemsOnlyVitaminC(items));





// 3. Get all items containing Vitamin A.

const itemsVitaminA = (items) => {

  const arrayOfitemsVitaminA = items.filter((item) => {

    return item.contains.includes("Vitamin A");
  });

  return arrayOfitemsVitaminA;
};

console.log(itemsVitaminA(items));






// 4. Group items based on the Vitamins that they contain in the following format:
//     {
//         "Vitamin C": ["Orange", "Mango"],
//         "Vitamin K": ["Mango"],
//     }
//     and so on for all items and all Vitamins.

const groupItems = (items) => {

  const groupObject = {};

  items.map((item) => {

    let arr = item.contains.split(",");
              
    arr.map(ele => {

      let trimmedEle = ele.trim();

      if (trimmedEle in groupObject) {

        groupObject[trimmedEle].push(item.name);

      } else {

        groupObject[trimmedEle] = [];

        groupObject[trimmedEle].push(item.name);
      }
    })
    });
  return groupObject;
};

console.log(groupItems(items));





// 5. Sort items based on number of Vitamins they contain.

const sortItems = (items) => {

  const sortedItems = items.map((item) => {

      let array = item.contains.split(",");

      return [item.name, array.length];

    }).sort((a, b) => {

      return a[1] - b[1];
    });

  return Object.fromEntries(sortedItems);
};

console.log(sortItems(items));
