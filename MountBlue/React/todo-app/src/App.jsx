import { Navigate, Route, Routes } from "react-router-dom";
import SignIn from "./Components/SignIn/SignIn";
import UserProfile from "./Components/UserProfile/UserProfile";
import ProjectDetails from "./Components/ProjectDetails/ProjectDetails";
import SignUp from "./Components/SignUp/SignUp";
import "./App.css";
import PageNotFound from "./Components/PageNotFound/PageNotFound";
import { setAuthToken } from "./Components/API/apiService";

function App() {

  const storedToken = localStorage.getItem("authToken");

  setAuthToken(storedToken);
  return (
    <>
      <Routes>
        <Route path="/" element={<Navigate to={"/signIn"} />} />

        <Route path="/signIn" element={<SignIn />} />

        <Route path="/signUp" element={<SignUp />} />

        <Route path="/todo/:username" element={<UserProfile />} />

        <Route path="/todo/project/:projectId" element={<ProjectDetails />} />

        <Route path="*" element={<PageNotFound />} />
      </Routes>
    </>
  );
}

export default App;
