import React, { useState } from "react";
import { Checkbox, Typography, Paper, IconButton } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import ConfirmDelete from "../ConfirmDelete/ConfirmDelete";

const Task = ({ task, onDeleteTask, onTaskCompletion }) => {
  const [deleteDialogOpen, setDeleteDialogOpen] = useState(false);

  const handleCheckboxChange = (event) => {
    const value = event.target.checked;
    onTaskCompletion(task.id, value);
  };

  const handleOpenDeleteDialog = () => {
    setDeleteDialogOpen(true);
  };

  const handleCloseDeleteDialog = () => {
    setDeleteDialogOpen(false);
  };

  const handleDeleteConfirmed = () => {
    onDeleteTask(task.id);
    handleCloseDeleteDialog();
  };

  return (
    <Paper
      elevation={1}
      style={{
        padding: "8px",
        marginBottom: "8px",
        display: "flex",
        alignItems: "center",
      }}
    >
      <Checkbox onChange={handleCheckboxChange} checked={task.completed} />
      <div style={{ flexGrow: 1 }}>
        <Typography variant="body1">
          {task ? task.task_description : null}
        </Typography>
      </div>
      <IconButton aria-label="Delete" onClick={handleOpenDeleteDialog}>
        <DeleteIcon />
      </IconButton>

      <ConfirmDelete
        open={deleteDialogOpen}
        onClose={handleCloseDeleteDialog}
        onConfirm={handleDeleteConfirmed}
        title={task ? task.task_description : null}
      />
    </Paper>
  );
};

export default Task;
