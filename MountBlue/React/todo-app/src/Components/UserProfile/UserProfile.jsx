import React, { useState, useEffect } from "react";
import Project from "../Project/Project";
import AddProject from "../AddProject/AddProject";
import { fetchProjects, createProject, deleteProject } from "../API/apiService";
import { Container } from "@mui/material";

const UserProfile = () => {
  const [projects, setProjects] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    fetchUserProjects();
  }, []);

  const fetchUserProjects = async () => {
    try {
      const projects = await fetchProjects();
      setProjects(projects);
      setLoading(false);
    } catch (error) {
      console.error("Error fetching projects:", error);
    }
  };

  const handleCreateProject = async (newProject) => {
    try {
      const createdProject = await createProject(newProject);
      // console.log("newProject",createdProject);
      setProjects((prevProjects) => {
        // console.log(prevProjects)
        return [...prevProjects, createdProject];
      });
    } catch (error) {
      console.error("Error creating project:", error);
    }
  };

  const handleDeleteProject = async (projectId) => {
    try {
      await deleteProject(projectId);
      setProjects((prevProjects) =>
        prevProjects.filter((project) => project.id !== projectId)
      );
    } catch (error) {
      console.error("Error deleting project:", error);
    }
  };

  if (loading) {
    return <p>Loading projects...</p>;
  }

  return (
    <Container>
      <h2>User Profile</h2>
      <AddProject onCreateProject={handleCreateProject} />
      <h3>Projects:</h3>
      {projects.length ? (
        projects.map((project) => (
          <Project
            key={project.id}
            project={project}
            onDeleteProject={handleDeleteProject}
          />
        ))
      ) : (
        <p>No Projects Found</p>
      )}
    </Container>
  );
};

export default UserProfile;
