import React, { useState } from "react";
import { TextField, Button, Stack } from "@mui/material";

const AddProject = ({ onCreateProject }) => {
  const [newProjectTitle, setNewProjectTitle] = useState("");
  const [newProjectDescription, setNewProjectDescription] = useState("");

  const handleCreateProject = () => {
    if (newProjectTitle && newProjectDescription) {
      const newProject = {
        title: newProjectTitle,
        description: newProjectDescription,
      };
      onCreateProject(newProject);
      setNewProjectTitle("");
      setNewProjectDescription("");
    }
  };

  return (
    <>
      <Stack direction="row" spacing={1}>
        <TextField
          label="Project Title"
          variant="outlined"
          value={newProjectTitle}
          onChange={(e) => setNewProjectTitle(e.target.value)}
        />
        <TextField
          label="Project Description"
          variant="outlined"
          value={newProjectDescription}
          onChange={(e) => setNewProjectDescription(e.target.value)}
        />
        <Button
          variant="contained"
          color="primary"
          size="small"
          onClick={handleCreateProject}
        >
          Create Project
        </Button>
      </Stack>
    </>
  );
};

export default AddProject;
