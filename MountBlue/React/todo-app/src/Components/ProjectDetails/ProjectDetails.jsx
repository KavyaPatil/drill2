import React, { useEffect, useState } from "react";
import { Typography, Paper } from "@mui/material";
import { useParams } from "react-router-dom";
import { fetchProjectById } from "../API/apiService";
import Task from "../Tasks/Tasks";
import AddTask from "../AddTask/AddTask";
import { createTask, deleteTask, updateTask } from "../API/apiService";

const ProjectDetails = () => {
  const { projectId } = useParams();
  const [project, setProject] = useState();
  const [tasks, setTasks] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getProjectById();
  }, []);

  const getProjectById = async () => {
    try {
      const response = await fetchProjectById(projectId);
      // console.log(response.tasks);
      setProject(response);
      setTasks(response.tasks);
      setLoading(false);
    } catch (err) {
      console.log(err, "wkmn");
    }
  };

  const handleCreateTask = async (newTask) => {
    try {
      const response = await createTask(projectId, newTask);
      // console.log("newTask", response);
      setTasks((previosTasks) => {
        return [...previosTasks, response];
      });
    } catch (error) {
      console.log(error);
    }
  };

  const handleDeleteTask = async (taskId) => {
    try {
      const response = await deleteTask(projectId, taskId);
      // console.log(response);

      setTasks((prevTasks) => prevTasks.filter((task) => task.id !== taskId));
    } catch (error) {
      console.log(error);
    }
  };

  const handleTaskCompletion = async (taskId, completed) => {
    try {
      const response = await updateTask(projectId, taskId, {
        completed,
      });
      // console.log(response);
      const updatedTasks = tasks.map((task) => {
        if (task.id === taskId) {
          return { ...task, completed: completed };
        } else {
          return task;
        }
      });
      setTasks(updatedTasks);
    } catch (error) {
      console.log(error);
    }
  };

  if (loading) {
    return <p>Loading...</p>;
  }

  return (
    <>
      <div>
        {" "}
        <Typography variant="h4">{project.title} Details</Typography>
        <Paper elevation={3} style={{ padding: "16px", marginBottom: "16px" }}>
          <Typography>{project.description}</Typography>
          <AddTask onCreateTask={handleCreateTask} />
          <Typography variant="subtitle1">Tasks:</Typography>
          {tasks.length ? (
            tasks.map((task, index) => {
              return (
                <Task
                  key={index}
                  task={task}
                  onDeleteTask={handleDeleteTask}
                  onTaskCompletion={handleTaskCompletion}
                />
              );
            })
          ) : (
            <p>No tasks Found..</p>
          )}
        </Paper>{" "}
      </div>
    </>
  );
};

export default ProjectDetails;
