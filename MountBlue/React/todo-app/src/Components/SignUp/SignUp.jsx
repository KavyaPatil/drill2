import React, { useState } from "react";
import { TextField, Button, Container, Typography } from "@mui/material";
import { signUp } from "../API/apiService";
import { Link, useNavigate } from "react-router-dom";

const SignUp = () => {
  const [formData, setFormData] = useState({
    name: "",
    username: "",
    email: "",
    phoneNumber: "",
    password: "",
    confirmPassword: "",
  });
  const [showWarn, setShowWarn] = useState(true);
  const navigate = useNavigate();

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const userData = {
      name: formData.name,
      username: formData.username,
      email: formData.email,
      phone_number: formData.phoneNumber,
      password: formData.password,
    };
    if (formData.password !== formData.confirmPassword) {
      return;
    } else {
      setShowWarn(false);
    }

    try {
      const response = await signUp(userData);
      // console.log(response);

      navigate(`/todo/${response.username}`);
    } catch (err) {
      console.log(err);
    }

    // console.log(formData);
  };

  return (
    <Container sx={{ width: "500px" }}>
      <Typography variant="h4">Signup</Typography>
      <form onSubmit={handleSubmit}>
        <TextField
          label="Name"
          variant="outlined"
          fullWidth
          name="name"
          value={formData.name}
          onChange={handleChange}
          margin="normal"
        />
        <TextField
          label="Username"
          variant="outlined"
          fullWidth
          name="username"
          value={formData.username}
          onChange={handleChange}
          margin="normal"
        />
        <TextField
          label="Email"
          variant="outlined"
          fullWidth
          type="email"
          name="email"
          value={formData.email}
          onChange={handleChange}
          margin="normal"
        />
        <TextField
          label="Phone Number"
          variant="outlined"
          fullWidth
          name="phoneNumber"
          value={formData.phoneNumber}
          onChange={handleChange}
          margin="normal"
        />
        <TextField
          label="Password"
          variant="outlined"
          fullWidth
          type="password"
          name="password"
          value={formData.password}
          onChange={handleChange}
          margin="normal"
        />
        <TextField
          label="Confirm Password"
          variant="outlined"
          fullWidth
          type="password"
          name="confirmPassword"
          value={formData.confirmPassword}
          onChange={handleChange}
          margin="normal"
          helperText={showWarn ? "password must match" : ""}
        />
        <Button
          type="submit"
          variant="contained"
          color="primary"
          fullWidth
          sx={{ margin: "20px 0px" }}
        >
          Sign Up
        </Button>
        <Link to="/signIn">Already have a acccount? SignIn</Link>
      </form>
    </Container>
  );
};

export default SignUp;
