import React from "react";
import { Typography, Button } from "@mui/material";
import { Link } from "react-router-dom";

const PageNotFound = () => {
  return (
    <div style={{ textAlign: "center", padding: "50px" }}>
      <Typography variant="h2">Page Not Found</Typography>
      <Typography variant="body1">
        The requested page does not exist.
      </Typography>
      <Link to="/">
        <Button
          variant="contained"
          color="primary"
          style={{ marginTop: "20px" }}
        >
          Go to Home Page
        </Button>
      </Link>
    </div>
  );
};

export default PageNotFound;
