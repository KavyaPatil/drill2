import axios from "axios";

const API_BASE_URL = "https://todo-api-kavya.onrender.com/";

export const signUp = async (userData) => {
  try {
    const response = await axios.post(`${API_BASE_URL}api/user`, userData);
    const token = response.data.token;
    // console.log(token, response.data.token);
    setAuthToken(token);
    return token;
  } catch (error) {
    throw error;
  }
};

export const setAuthToken = (token) => {
  if (token) {
    localStorage.setItem("authToken", token);
    axios.defaults.headers.common["Authorization"] = token;
  } else {
    delete axios.defaults.headers.common["Authorization"];
    localStorage.removeItem("authToken");
  }
};

export const login = async (credentials) => {
  try {
    const response = await axios.post(
      `${API_BASE_URL}api/user/login`,
      credentials
    );
    const token = response.data.token;
    // console.log(token, response.data.token);
    setAuthToken(token);
    return token;
  } catch (error) {
    throw error;
  }
};

// Function to log out
export const logout = () => {
  setAuthToken(null);
};

export const fetchProjects = async () => {
  try {
    const response = await axios.get(`${API_BASE_URL}api/project`);
    // console.log(response);
    return response.data.data;
  } catch (error) {
    throw error.response.data;
  }
};
[0].tasks;

export const createProject = async (projectData) => {
  try {
    const response = await axios.post(
      `${API_BASE_URL}api/project`,
      projectData
    );
    // console.log(response);
    return response.data.data;
  } catch (error) {
    throw error.msg;
  }
};

export const fetchProjectById = async (projectId) => {
  try {
    const response = await axios.get(`${API_BASE_URL}api/project/${projectId}`);
    return response.data.data;
  } catch (error) {
    throw error;
  }
};

export const updateProject = async (projectId, projectData) => {
  try {
    const response = await axios.put(
      `${API_BASE_URL}api/project/${projectId}`,
      projectData
    );
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};

export const deleteProject = async (projectId) => {
  try {
    const response = await axios.delete(
      `${API_BASE_URL}api/project/${projectId}`
    );
    return response.data;
  } catch (error) {
    throw error.response.data;
  }
};

export const createTask = async (projectId, taskData) => {
  try {
    const response = await axios.post(
      `${API_BASE_URL}api/project/${projectId}/tasks`,
      taskData
    );
    return response.data.data;
  } catch (error) {
    throw error;
  }
};

export const updateTask = async (projectId, taskId, taskData) => {
  try {
    const response = await axios.put(
      `${API_BASE_URL}api/project/${projectId}/tasks/${taskId}`,
      taskData
    );
    // console.log("updatedTask", response);
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const deleteTask = async (projectId, taskId) => {
  try {
    const response = await axios.delete(
      `${API_BASE_URL}api/project/${projectId}/tasks/${taskId}`
    );
    return response.data;
  } catch (error) {
    throw error;
  }
};
