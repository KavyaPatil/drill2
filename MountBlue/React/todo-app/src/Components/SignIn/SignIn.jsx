import React, { useState } from "react";
import { TextField, Button, Container, Typography } from "@mui/material";
import { login } from "../API/apiService";
import { Link, useNavigate } from "react-router-dom";

const SignIn = () => {
  const [formData, setFormData] = useState({
    username: "",
    password: "",
  });
  const navigate = useNavigate();

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await login(formData);
      // console.log("usercredentials",response);
      navigate(`/todo/${formData.username}`);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Container sx={{ width: "500px" }}>
      <Typography variant="h4" gutterBottom>
        Sign In
      </Typography>
      <form onSubmit={handleSubmit}>
        <TextField
          label="Username"
          variant="outlined"
          fullWidth
          name="username"
          value={formData.username}
          onChange={handleChange}
          margin="normal"
        />
        <TextField
          label="Password"
          variant="outlined"
          fullWidth
          type="password"
          name="password"
          value={formData.password}
          onChange={handleChange}
          margin="normal"
        />
        <Button
          type="submit"
          variant="contained"
          color="primary"
          fullWidth
          sx={{ margin: "20px 0px" }}
        >
          Sign In
        </Button>
        <Link to="/signUp">Create a new account? signup</Link>
      </form>
    </Container>
  );
};

export default SignIn;
