import React, { useState } from "react";
import { TextField, Button, Paper, Stack } from "@mui/material";

const AddTask = ({ onCreateTask }) => {
  const [newTaskDescription, setNewTaskDescription] = useState("");

  const handleCreateTask = () => {
    if (newTaskDescription) {
      const newTask = {
        task_description: newTaskDescription,
        completed: false,
      };
      onCreateTask(newTask);
      setNewTaskDescription("");
    }
  };

  return (
    <Paper elevation={1} style={{ padding: "16px", marginBottom: "16px" }}>
      <h3>Create New Task:</h3>
      <Stack direction="row" spacing={1}>
        <TextField
          label="Task Description"
          variant="outlined"
          value={newTaskDescription}
          onChange={(e) => setNewTaskDescription(e.target.value)}
          rows={4}
        />
        <Button
          variant="contained"
          color="primary"
          onClick={handleCreateTask}
          size="small"
        >
          Create Task
        </Button>
      </Stack>
    </Paper>
  );
};

export default AddTask;
