import React, { useState } from "react";
import { Paper, Typography, Button, IconButton, Stack } from "@mui/material";
import ConfirmDelete from "../ConfirmDelete/ConfirmDelete";
import { Link } from "react-router-dom";
import DeleteIcon from "@mui/icons-material/Delete";

const Project = ({ project, onDeleteProject }) => {
  const [deleteDialogOpen, setDeleteDialogOpen] = useState(false);

  const handleOpenDeleteDialog = () => {
    setDeleteDialogOpen(true);
  };

  const handleCloseDeleteDialog = () => {
    setDeleteDialogOpen(false);
  };

  const handleDeleteConfirmed = () => {
    onDeleteProject(project.id);
    handleCloseDeleteDialog();
  };

  return (
    <Paper elevation={3} style={{ padding: "16px", marginBottom: "16px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <Typography variant="h5">{project.title}</Typography>
        <IconButton
          aria-label="Delete"
          onClick={handleOpenDeleteDialog}
          size="small"
        >
          <DeleteIcon />
        </IconButton>
      </div>
      <Typography variant="body1" style={{margin:"20px 0px"}}>{project.description}</Typography>

      <Link to={`/todo/project/${project.id}`}>
        <Button variant="contained" color="primary" size="small">
          Project Details
        </Button>
      </Link>
      <ConfirmDelete
        open={deleteDialogOpen}
        onClose={handleCloseDeleteDialog}
        onConfirm={handleDeleteConfirmed}
        title={project.title}
      />
    </Paper>
  );
};

export default Project;
